*** Settings ***
Documentation  This file contains keywords related to login page
Resource       Config/login_page/login_page_locators.robot

*** Variables ***
${headless_chrome_browser}      headlesschrome
${chrome}       chrome

*** Keywords ***
Launch Browser
    [Documentation]   This keyword is used to launch browser with site address
    Open Browser  http://automationpractice.com/index.php   ${headless_chrome_browser}

Verify Elements On Login Page
    [Documentation]  This keyword will verify elements are present on page
    Page Should Contain Element     ${Sign_In_Locator}
    Page Should Contain Element     ${Your_Logo_Locator}
    Page Should Contain Element     ${Women_Section_Locator}
    Page Should Contain Element     ${Dresses_Locator}
    Page Should Contain Element     ${TShirts_Locator}
    Page Should Contain Element     ${View_My_Shopping_Cart_Locator}
    Page Should Contain Element     ${Search_Box_Locator}