*** Settings ***
Documentation  This file contains keywords related to women section
Resource  Config/women_section/women_section_locators.robot

*** Keywords ***
Verify Tops And Dresses Sections On Women Page
    [Documentation]  This keyword will verify elements are present on page
    Wait Until Page Contains Element     ${Tops_Locator}     4Sec
    Wait Until Page Contains Element     ${Dresses_Locator}  4Sec


Verify Tops Section In Women Page
    [Documentation]  This keyword will verify tops section on women page
    Wait Until Page Contains Element     ${TShirts_Locator}     4Sec
    Wait Until Page Contains Element     ${Blouse_Locator}  4Sec