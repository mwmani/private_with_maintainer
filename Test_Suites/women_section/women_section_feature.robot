*** Settings ***
Library     SeleniumLibrary
Library     women.py
Resource    Config/login_page/login_page_locators.robot
Resource    Utilities/login_page_functionalties/login_page_functionality.robot
Resource    Config/women_section/women_section_locators.robot
Resource    Utilities/women_section/women_section_functionality.robot
Suite Setup    Run Keyword    Launch Browser
Suite Teardown     Close Browser

*** Variables ***

*** Test Cases ***
TC1 Check Women Section Page
    [Documentation]  This test case will verify elements on tshirts section
    [Tags]      Sanity
    Click Element       ${Women_Section_Locator}
    Title Should Be     ${Women_Section_Page_Title}
    Verify Tops And Dresses Sections On Women Page
    func


TC2 Check Tops Section On Women Page
    [Documentation]  This test case will verify tops section
    [Tags]      Sanity
    Click Element       ${Women_Section_Locator}
    Title Should Be     ${Women_Section_Page_Title}
    Verify Tops And Dresses Sections On Women Page
    Verify Tops Section In Women Page
