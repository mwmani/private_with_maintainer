*** Settings ***
Library   SeleniumLibrary
Resource  Config/login_page/login_page_locators.robot
Resource  Utilities/login_page_functionalties/login_page_functionality.robot

*** Variables ***
${site_address}     http://automationpractice.com/index.php
#${browser}          headlesschrome
${browser}          chrome

*** Test Cases ***
TC1 Login Scenario
    [Documentation]  This test case will verify elements on login page of the site
    [Tags]   Regression
    Open Browser  ${site_address}    ${browser}
    Title Should Be     ${Login_Page_Title}
    Verify Elements On Login Page
    Close Browser

*** Keywords ***
