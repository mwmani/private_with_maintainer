Introduction:

This project is to test the functionality of "http://automationpractice.com/index.php?id_category=3&controller=category" site.

We are using Robot framework with python to test the application.

1) Introduction to Robot Framework:
   Robot Framework is a generic open source automation framework.
   It can be used for test automation and robotic process automation (RPA).
   Robot Framework is supported by Robot Framework Foundation.
   Many industry-leading companies use the tool in their software development.
   Robot Framework is open and extensible.
   Robot Framework can be integrated with virtually any other tool to create powerful and flexible automation solutions.
   Robot Framework is free to use without licensing costs.
   Robot Framework has an easy syntax, utilizing human-readable keywords.
   Its capabilities can be extended by libraries implemented with Python, Java or many other programming languages.
   Robot Framework has a rich ecosystem around it, consisting of libraries and tools that are developed as separate projects.

2) Environment setup
    - Install Python
    - Install Pycharm IDE
    - Install Selenium package
    - Install Robot framework package
    - Install Robotframework Selenium Library package
    - Install Hyper RobotFramework Support plugin for pycharm

We are also using Gitlab for version controlling and CI/CD deployments.