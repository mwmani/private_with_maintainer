*** Settings ***
Documentation  This file contains locators for elements on login page

*** Variables ***
${Login_Page_Title}       My Store
${Sign_In_Locator}        class:login
${Your_Logo_Locator}      xpath://*[@class="logo img-responsive"]
${Women_Section_Locator}  css:#block_top_menu > ul > li:nth-child(1) > a
${Dresses_Locator}        xpath://*[@id="block_top_menu"]/ul/li[2]/a
${TShirts_Locator}        xpath://*[@id="block_top_menu"]/ul/li[3]/a
${View_My_Shopping_Cart_Locator}    xpath://*[@title="View my shopping cart"]
${Search_Box_Locator}     id:search_query_top
