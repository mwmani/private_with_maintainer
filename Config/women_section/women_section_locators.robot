*** Settings ***
Documentation  This file contains locators for elements on women section

*** Variables ***
${Women_Section_Page_Title}       Women - My Store
${Women_Section_Locator}          xpath://*[@id="block_top_menu"]/ul/li[1]/a
${Tops_Locator}                   css:#categories_block_left > div > ul > li:nth-child(1) > a
${Dresses_Locator}                css:#categories_block_left > div > ul > li.last > a
${Catalog_Locator}                xpath://*[@id="layered_block_left"]/p
${TShirts_Locator}                css:#categories_block_left > div > ul > li:nth-child(1) > ul > li:nth-child(1) > a
${Blouse_Locator}                 xpath://*[@id="categories_block_left"]/div/ul/li[1]/ul/li[2]/a
${T-shirts_Page_Title}            T-shirts - My Store
${Blouse_Page_Title}              Blouses - My Store
