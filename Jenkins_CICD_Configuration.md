CI/CD pipeline configuration of Gitlab code with Jenkins:

This document gives the details about the how we can configure Gitlab code to CI/CD pipeline in Jenkins.
We have configured the Pipeline in a way that it runs all the test cases in the test suite whenever a push of the code is initiated into git repository.
Below are the steps by which we can configure gitlab repository to CI/CD Jenkins pipeline.

Jenkins Installation on Windows:
Step 1 : Check if Java is installed on your system
Step 2 : Download Jenkins.war file
Step 3 : Goto cmd prompt and run below command:
   java -jar jenkins.war --httpPort=8080
Step 4 : On browser goto http://localhost:8080
Step 5 : Provide admin password and complete the setup

How to setup Git on Jenkins:
Step 1 : Goto Manage Jenkins ＞ Manage Plugins
Step 2 : Check if git is already installed in Installed tab
Step 3 : Else goto Available tab and search for Git, GitLab Plugin, GIT server Plugin, 
         Git client plugin,GitLab Branch Source Plugin,Gitlab API Plugin,GitLab Authentication
Step 4 : Install Git, GitLab Plugin, GIT server Plugin, Git client plugin,GitLab Branch Source Plugin,Gitlab API Plugin,GitLab Authentication
Step 5 : Check git option is present in Job Configuration

Create the first Job on Jenkins
How to connect to Git Remote Repository in Jenkins (GitLab)
Step 1 : Get the url of the remote repository
Step 2 : Add the git credentials on Jenkins
Step 3 : In the jobs configuration goto SCM and provide git repository url in git section
Step 4 : Add the credentials
Step 5 : Run job and check if the repository is cloned

Build ＞ Deploy ＞ Test ＞ Release
Jenkins : Pipeline as a code
Step 1 : Start Jenkins
Step 2 : Install Pipeline Plugin
Step 3 : Create a new job
Step 4 : Create or get Jenkins file in Pipeline section
Step 5 : Run and check the output

Jenkins : Pipeline for our project 
Step 1 : Create a new job or use existing job (type : Pipeline)
Step 2 : Create a repository on GitLab
Step 3 : Select Configure option from the left pane on Jenkins 
Step 4 : Under the Source Code Management section Select Git option
                 > Provide Repository URL
                 > Provide Gitlab credential in Credentials section
                 > Under Branches to build, provide which branch to be executed in Branch Specifier (blank for 'any') section
                 > Under Build Triggers section select Build when a change is pushed to GitLab. GitLab webhook URL: http://localhost:8080/project/AB_Project_Jenkins_Gitlab
                       > Select Push Events under Enabled GitLab triggers section
                       > Also select Opened Merge Request Events under Enabled GitLab triggers section
                       > Select Poll SCM and provide the schedule as per your requirement 
Step 5 : Build Section can be left blank if no command is to be executed 
Step 6 : Under Post-build Actions > select Publish Robot Framework Test Results from Add post-build action drop down
Step 7 : If you want to generate Robot test results then install Robot Framework plugin from manage plugins section in Manage Jenkins 
Step 8 : Save & Run
Step 9 : Check the job is executed 
Step 10: If you want to check if the pipeline gets triggered after we push the code then follow below steps:
           > Go to pycharm where your code from gitlab repository is opened 
           > Make changes in your code 
           > Push the code to the git repository
           > Check on the jenkins if the pipeline is triggered(You will see a job is running on jenkins) 